#include<stdio.h>
void swap(int *a,int *b);
int main()
{
    int num1,num2;
    printf("\n Enter the First Number:");
    scanf("%d",&num1);
    printf("\n Enter the Second Number:");
    scanf("%d",&num2);
    printf("Before Swapping the two numbers are %d and %d\n",num1,num2);
    swap(&num1,&num2);
    printf("After Swapping the two numbers are %d and %d\n",num1,num2);
    return 0;
}
void swap( int *a, int *b)
{
    int temp;
    temp=*a;
    *a=*b;
    *b=temp;
}