#include<stdio.h>
int main()
{
  int c, d, p, q, m, n, k, tot = 0;
  int first[10][10], second[10][10], mul[10][10];

  printf("Enter  the number of rows and columns for first matrix \n ");
  scanf("%d%d", &m, &n);

  printf(" Insert elements for the first matrix : \n ");
  for (c = 0; c < m; c++)
    for (d = 0; d < n; d++)
      scanf("%d", &first[c][d]);

  printf("Enter the number of rows and columns for second matrix\n");
  scanf(" %d %d", &p, &q);

  if (n != p)
    printf(" The given matrices cannot be multiplied with each other. \n ");
  else
  {
    printf(" Insert elements for the second matrix \n ");

    for (c = 0; c < p; c++)
      for (d = 0; d < q; d++)
        scanf("%d", &second[c][d] );

    for (c = 0; c < m; c++) 
    {
      for (d = 0; d < q; d++) 
      {
        for (k = 0; k < p; k++) 
        {
          tot = tot + (first[c][k] * second[k][d]);
        }
        mul[c][d] = tot;
        tot = 0;
      }
    }

    printf(" The result of matrix multiplication  is: \n "); 
    for (c = 0; c < m; c++)
    {
      for (d = 0; d < q; d++)
        printf("%d \t", mul[c][d] );
      printf(" \n ");
    }
  }
  return 0;
}