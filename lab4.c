#include<stdio.h>
int main()    
{
    int arr[100], i, n, ele, pos;
    printf("Enter size of the array : ");
    scanf("%d", &n);
    printf("Enter array elements:");
    for(i=0; i<n; i++)
    {
    	scanf("%d", &arr[i]);
    }
    printf("Enter element to be insert : ");
    scanf("%d", &ele);
    printf("Enter position at which element to be insert: ");
    scanf("%d", &pos);
    for(i=n; i>pos; i--)
    {
    	arr[i] = arr[i-1];
    }
    arr[pos] = ele;
    printf("Array elements after insertion : "); 
    for(i=0; i<n+1; i++)
    {
    	 printf("%d\n", arr[i]);
    }
    return 0;
}