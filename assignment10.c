#include <stdio.h>
#include <stdlib.h>
int main()
{
    FILE *fp,*fs;
    char ch,filename[20];
    printf("Enter the file name\n");
    gets(filename);
    fp=fopen(filename,"r");
    if(fp==NULL)
    {
        printf("Could not open file\n");
        exit(1);    
    }
    fs=fopen("TEXT.txt","w");
    while(1)
    {
        ch=fgetc(fp);
        if(ch==EOF)
            break;
        else
            fputc(ch,fs);
    }
    fclose(fp);
    fclose(fs);
    return 0;
}