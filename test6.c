#include<stdio.h>
int main()
{
	int mat1[3][3],mat2[3][3];
	int i,j;
	printf("Enter elements of matrix:\n");
	for(i=0;i<3;i++)
	{
		for(j=0;j<3;j++)
		{
			scanf("%d",&mat1[i][j]);
		}
	}
	for(i=0;i<3;i++)
	{
		for(j=0;j<3;j++)
		{
			mat2[i][j]=mat1[j][i];
		}
	}
	printf("The transpose of Matrix is:\n");
	for(i=0;i<3;i++)
	{
		for(j=0;j<3;j++)
		{
			printf("%d\t",mat2[i][j]);
		}
		printf("\n");
	}
    return 0;
}