#include <stdio.h>
int main()
{
    int n, dig, rev=0;
    printf("Enter a number:");
    scanf("%d",&n);
    while(n!=0)
    {
        dig=n%10;
        rev=rev*10+dig;
        n=n/10;
    }
    printf("Reverse=%d",rev);
    return 0;
}

Output
Enter the number:564
Reverse=465