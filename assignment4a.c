#include <stdio.h>
void swap_pass_value(int a, int b)
{
 int temp;
 temp = a;
 a = b;
 b = temp;
 printf ("The value of a and b after swapping inside the function %d, %d\n",a,b);
}
void swap_pass_ref(int *a, int*b)
{
 int temp;
 temp = *a;
 *a = *b;
 *b = temp;
 printf("The value of a and b after swapping inside the function %d,  %d\n",*a,*b);
}
int main()
{
 int x,y;
 printf("Enter the numbers\n");
 scanf("%d%d",&x,&y);
 swap_pass_value(x,y);
  printf("The values of a and b after calling swap in main function %d, %d\n",x,y);
 swap_pass_ref(&x,&y);
  printf("The values of a and b after calling swap in main function %d, %d\n",x,y);
 return 0;
}