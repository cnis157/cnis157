#include<stdio.h>
int main()
{
	char word[100];
	int j,i=0,c=1,len=0;
	printf("Enter a word to check for whether it is palindrome or not\n");
	scanf("%s",word);
	while(word[i]!='\0')
	{
		len++;
		i++;
	}
	for(i=0,j=len-1;i<=j;i++,j--)
	{
		if(word[i]==word[j])
			c=0;
		else
		{
			c=1;
			break;
		}
	}
	if(c==0)
		printf("Palindrome word");
	else
		printf("Not a palindrome word");
	return 0;
}
